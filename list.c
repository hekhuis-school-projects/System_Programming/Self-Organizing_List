/*************************************************************
 * Project 2: A Self-Organizing List
 * CIS 361: System Programming
 * GVSU Winter 2017
 *
 * This class creates a self-organizing linked list meaning
 * everytime a search is performed on it, that search goes to
 * the front of list to reduce successive searches for the
 * same identfier. The list holds nodes which contain an
 * identfier and number of occurrences for that identfier
 * (how many times it has been searched). If an identfier is
 * searched for it and it is not in the list, a new node is
 * created for that identfier and put at the front of the
 * list. Contains functions for initalizing, freeing, searching,
 * and printing the list. Also contains functions for pushing,
 * popping, removing, and getting nodes, as well as a function
 * to check if two identifiers are equal.
 *
 * @author Kyle Hekhuis
 ************************************************************/

#include "list.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/*********************************
 * Initializes a linked list.
 *
 * @param list pointer to list to
 * 			   initialize
 ********************************/
void initialize(LinkedList* list) {
	list->size = 0;
	list->head = NULL;
}

/***************************************
 * Frees the memory allocated for the 
 * linked list
 * 
 * @param list pointer to list to free
 **************************************/
void freeList(LinkedList* list) {
	Node* current = list->head;
	while (current != NULL) {
		Node* temp = current->next;
		free(current->identifier);
		free(current);
		current = temp;
	}
	list->size = 0;
	list->head = NULL;
}

/******************************************
 * Prints a linked list to the specified
 * FILE*.
 *
 * @param list pointer to linked list
 * 			   to print
 * @param output FILE* for output of print
 *****************************************/
void printList(LinkedList* list, FILE* output) {
	Node* current = list->head;
	int count = 1;
	fprintf(output, "List size: %d\n", list->size);
	while (current != NULL) {
		fprintf(output, "==============================\n"
			   "Node %d:\nIdentifier: %s\nOccurences: %d\n"
			   "==============================\n",
			   count, current->identifier, current->occurrences);
		current = current->next;
		count++;
	}
}

/**************************************************
 * Searches a linked list for a node with the
 * passed identifier. If it finds it, it moves
 * it to the front and increases the occurence by
 * 1. If the identifier is not in the list, a
 * new node is inserted into the front of the list
 * with the searched for identifier. The occurence
 * of that identifier is set to 1.
 *
 * @param list pointer to list to search
 * @param pIdentifier identifier to search for
 *************************************************/
void search(LinkedList* list, char* pIdentifier) {
	Node* current = list->head;
	while (current != NULL) {
		if (equals(pIdentifier, current->identifier)) {
			current->occurrences++;
			moveToFront(list, current);
			return;
		}
		current = current->next;
	}
	push(list, pIdentifier);
}

/*************************************************
 * Creates a new node with the passed identifier
 * and adds it to the front of the passed linked
 * list.
 *
 * @param list pointer to linked list to add to
 * @param pIdentifier identifier to add to list
 * @return TRUE if identifier successfully
 *         added to linked list. FALSE otherwise.
 ************************************************/
Boolean push(LinkedList* list, char* pIdentifier) {
	Node* newNode;
	newNode = malloc(sizeof(Node));
	if (newNode == NULL) {
		return FALSE;
	}
	newNode->identifier = (char*)malloc(strlen(pIdentifier) * sizeof(char));
	if (newNode->identifier == NULL) {
		return FALSE;
	}
	strcpy(newNode->identifier, pIdentifier);
	newNode->occurrences = 1;
	newNode->next = list->head;
	list->head = newNode;
	list->size++;
	return TRUE;
}

/********************************************************
 * Pops the node corresponding to the passed identifier
 * from the passed linked list and puts it in the
 * passed node pointer.
 *
 * @param list linked list to pop from
 * @param pIdentifier identifier of node to pop
 * @param n node pointer to store popped node in
 * @return TRUE if node popped successfully. FALSE
 * 		   otherwise.
 *******************************************************/
Boolean pop(LinkedList* list, char* pIdentifier, Node* n) {
	Node* current = list->head;
	while (current != NULL) {
		if (equals(pIdentifier, current->next->identifier)) {
			n = current->next;
			current->next = n->next;
			n->next = NULL;
			list->size--;
			return TRUE;
		}
		current = current->next;
	}
	return FALSE;
}

/*****************************************************
 * Removes the node with the passed identifier if it
 * exist in the passed linked list.
 *
 * @param list pointer to linked list to remove node
 * 			   from
 * @param pIdentifier identifier of node to remove
 ****************************************************/
void removeNode(LinkedList* list, char* pIdentifier) {
	Node* current = list->head;
	Node* temp;
	while (current != NULL) {
		if (equals(pIdentifier, current->next->identifier)) {
			temp = current->next;
			current->next = temp->next;
			temp->next = NULL;
			free(temp->identifier);
			free(temp);
			list->size--;
			return;
		}
		current = current->next;
	}
}

/***********************************************************
 * Gets the node with the passed identifier from the passed
 * linked link if it exist and stores the pointer to it in
 * the passed node pointer. It does not remove it from the
 * linked list.
 *
 * @param list pointer to linked list to get node from
 * @param pIdentifier identifier of node to get from list
 * @param n node pointer to store the gotten node in
 * @return TRUE if the node was successfully found in the
 * 		   list and stored in n. FALSE otherwise.
 **********************************************************/
Boolean getNode(LinkedList* list, char* pIdentifier, Node* n) {
	Node* current = list->head;
	while (current != NULL) {
		if (equals(pIdentifier, current->identifier)) {
			n = current;
			return TRUE;
		}
		current = current->next;
	}
	return FALSE;
}

/******************************************
 * Moves the passed node pointer to the
 * front of the passed linked list.
 *
 * @param list pointer to linked list to
 * 			   move node on
 * @param n node pointer to move to front
 *****************************************/
void moveToFront(LinkedList* list, Node* n) {
	//If already at front, return
	if (list->head == n) {
		return;
	}
	if (pop(list, n->identifier, n)) {
		n->next = list->head;
		list->head = n;
		list->size++;
	}
}

/************************************************
 * Checks to see if two identifiers are equal.
 *
 * @param identifier1 first identifer
 * @param identifier2 second identfier
 * @return TRUE if identifier1 and identifier 2
 * 		   are the same. FALSE otherwise.
 ***********************************************/
Boolean equals(char* identifier1, char* identifier2) {
	if (strcmp(identifier1, identifier2) == 0) {
		return TRUE;
	}
	return FALSE;
}