Self-Organizing List
Language: C
Author: Kyle Hekhuis (https://gitlab.com/hekhuisk)
Class: CIS 361- System Programming
Semester / Year: Winter 2017

This program creates a self-organizing list data structure. The list
allows a search function which moves the searched for term to the
front of the list so it's faster to find on successive searches. For full
specifications see 'Project 2 - A Self-Organizing List.pdf'.

Compile with either gcc or clang on Linux.