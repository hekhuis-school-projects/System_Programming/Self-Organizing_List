/*************************************************************
 * Project 2: A Self-Organizing List
 * CIS 361: System Programming
 * GVSU Winter 2017
 *
 * This program reads in a C code source file and parses
 * it for identifiers which for this project are specified
 * as a sequence of one ore more letters, digits, and
 * underscores, but it can't begin with a digit. Words in
 * a program comment or string / character constant are not
 * considered identifiers. Once the identifiers are found
 * they are added to a self-organizing linked list and then
 * the contents of the linked list are put into a passed
 * output file.
 *
 * @author Kyle Hekhuis
 ************************************************************/

#include "list.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <ctype.h>

int read_file(char* filename, char** buffer);
void parseString(char* buffer, int size, char* identList);
void addIdentifiers(LinkedList* list, char* identList);

/***********************************************************
 * Takes in command line arguments for C source file to
 * parse and an output file to put contents of linked list.
 * Runs the program to parse the file, add identifiers to
 * a linked list, and put contents of linked list into
 * the specified output file.
 **********************************************************/
int main(int argc, char** argv) {
	//Check if enough arguments passed
	if (argc < 3) {
		printf("Not enough arguments passed!\n"
			   "Argument 1 is file to read for identifiers"
			   " and argument 2 is the file to output the "
			   "linked list's contents to.\n");
		return 1;
	}
	//Linked list to hold identifiers
	LinkedList list;
	initialize(&list);
	//Holds contents of input file
	char* buffer;
	//Holds identifiers found in input file
	char* identList;
	//Read file and get size of it
	int size = read_file(argv[1], &buffer);
	//Check for read_file error
	if (size == -1) {
		fprintf(stderr, "File read error.\n");
		return 1;
	}
	//Know length of identifiers can't be bigger than
	//the size of the file so a safe size to allocate for
	//the identifier list is the same size as the file buffer.
	identList = malloc(size * sizeof(char));
	if (identList == NULL) {
		printf("Malloc error.\n");
		exit(1);
	}
	
	parseString(buffer, size, identList);
	addIdentifiers(&list, identList);
	
	//Print out the linked list to user specified file
	FILE* outFile;
	outFile = fopen(argv[2], "w");
	if (!outFile) {
		printf("Failure to open outfile.");
		exit(1);
	}
	printList(&list, outFile);
	fclose(outFile);
	
	free(buffer);
	free(identList);
	freeList(&list);
	
	return 0;
}

/**************************************************************
 * Reads a text file and puts the contents of it into a
 * buffer of characters. Returns the size of the file
 * which is also the size of the buffer.
 *
 * @param filename - filename of text file to read
 * @param buffer - pointer to buffer of characters
 * @return -1 for failure to open file, otherwise size of file
 **************************************************************/
int read_file(char* filename, char** buffer){
	FILE *fptr;
	fptr = fopen(filename, "r");
	if (!fptr) {
		return -1;
	}
	struct stat st;
	stat(filename, &st);
	int size = st.st_size;
	*buffer = malloc(size * sizeof(char));
	if (*buffer == NULL) {
		printf("Malloc error.");
		exit(1);
	}
	fread(*buffer, sizeof(char), size, fptr);
	fclose(fptr);

	return size;
}

/**************************************************************
 * Parses a string containing the contents of a file for
 * identifiers in the file. Does so using a switch and reading
 * the string char by char. Any identifiers found are stored
 * into the passed string of identifiers.
 *
 * @param buffer string of characters to parse
 * @param size size of buffer
 * @param identList string holding identifiers
 *************************************************************/
void parseString(char* buffer, int size, char* identList) {
	int state = 0;
	int pos = 0; //Position in identList
	for (int i = 0; i < size; i++) {
		switch(state) {
			case 0: //Check for start of identifier
				if (buffer[i] == '_' || isalpha(buffer[i])) {
					//Start of identifier found, add char to
					//identList and advance pos
					state = 1;
					identList[pos++] = buffer[i];
				} else if (buffer[i] == '\"') {
					//Start of "text" found
					state = 2;
				} else if (buffer[i] == '\'') {
					//Start of 'char' found
					state = 3;
				} else if (buffer[i] == '/') {
					//Start of a comment found
					state = 4;
				}
				break;
			case 1: //Check if identifier continues
				if (buffer[i] == '_' || isalnum(buffer[i])) {
					//Identifier continues, add char to identList
					//and advance pos
					identList[pos++] = buffer[i];
				} else {
					//Identifier done
					state = 0;
					identList[pos++] = '\n';
				}
				break;
			case 2: // "text" case
				if (buffer[i] == '\"') {
					//end of "text" found
					state = 0;
				}
				break;
			case 3: // 'char' case
				if (buffer[i] == '\'') {
					//end of 'char' found
					state = 0;
				}
				break;
			case 4: // / case
				if (buffer[i] == '/') {
					//Is a // comment
					state = 5;
				} else if (buffer[i] == '*') {
					//Is a /* */ comment
					state = 6;
				} else {
					//Not actually a comment
					state = 0;
				}
				break;
			case 5: // // case
				if (buffer[i] == '\n') {
					//End of // comment
					state = 0;
				}
				break;
			case 6: // */ case
				if (buffer[i] == '*') {
					int charAfter = i + 1;
					if (charAfter < size && buffer[charAfter] == '/') {
						//End of /* */ comment
						i++;
						state = 0;
					}
				}
				break;
		}
	}
}

/***************************************************************
 * Adds identifiers found in the file and located in the passed
 * identifier list to the passed linked list using the list
 * search function to make the list organize itself properly.
 *
 * @param list LinkedList to add identifiers to
 * @param identList string containing list of identifiers
 **************************************************************/
void addIdentifiers(LinkedList* list, char* identList) {
	char* token;
	token = strtok(identList, "\n");
	while (token != NULL) {
		search(list, token);
		token = strtok(NULL, "\n");
	}
}